var APP_DATA = {
  "scenes": [
    {
      "id": "0-puerta-de-entrada",
      "name": "Puerta de entrada",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 3.0565365940910887,
        "pitch": 0.021106772107998495,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": 3.0819621365843632,
          "pitch": 0.34271100136405686,
          "rotation": 6.283185307179586,
          "target": "1-entrada"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "1-entrada",
      "name": "Entrada",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 0.8811382011796454,
        "pitch": 0.12074655667034051,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": 1.352615815176728,
          "pitch": 0.19318630904397338,
          "rotation": 0,
          "target": "2-planta-baja---pasillo"
        },
        {
          "yaw": 0.07540284015316168,
          "pitch": -0.02609346997109796,
          "rotation": 5.497787143782138,
          "target": "11-escalera"
        },
        {
          "yaw": 3.1254278593689557,
          "pitch": 0.40930884613021234,
          "rotation": 0,
          "target": "0-puerta-de-entrada"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "2-planta-baja---pasillo",
      "name": "Planta baja - Pasillo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": -1.5425311010215772,
        "pitch": 0.088596854277597,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": -1.5801301984691953,
          "pitch": 0.2269889347941767,
          "rotation": 0,
          "target": "3-planta-baja---pasillo"
        },
        {
          "yaw": 1.6020369748158156,
          "pitch": 0.15601395822000264,
          "rotation": 0,
          "target": "1-entrada"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 2.279303913790476,
          "pitch": 0.17747196091406003,
          "title": "4011",
          "text": "Florencia Fiorentín<div>Mariano Pereira</div>"
        },
        {
          "yaw": -0.5533865666712394,
          "pitch": 0.22802934721033807,
          "title": "4010",
          "text": "Gustavo Real<div>Carlos Belmar</div>"
        },
        {
          "yaw": -2.164260300323342,
          "pitch": 0.18795139125832705,
          "title": "4013",
          "text": "Oficina de becarios"
        },
        {
          "yaw": -1.3586115395701537,
          "pitch": 0.12936684000140986,
          "title": "4012",
          "text": "Santiago Juncal<div>Melisa Girad</div>"
        }
      ]
    },
    {
      "id": "3-planta-baja---pasillo",
      "name": "Planta baja - Pasillo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 1.7188493126173956,
        "pitch": -0.03957180800963833,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": 1.5812094250069135,
          "pitch": 0.15225111813683512,
          "rotation": 0,
          "target": "4-planta-baja---pasillo"
        },
        {
          "yaw": -1.5512625436599414,
          "pitch": 0.20847993651961794,
          "rotation": 0,
          "target": "2-planta-baja---pasillo"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -2.860155979353946,
          "pitch": 0.2701126468845807,
          "title": "4012",
          "text": "Santiago Juncal<div>Melisa Girad</div>"
        },
        {
          "yaw": -1.0144034870097975,
          "pitch": 0.130670189530683,
          "title": "4013",
          "text": "Oficina de becarios"
        },
        {
          "yaw": -1.8143852484913356,
          "pitch": 0.17358776299313305,
          "title": "4010",
          "text": "Gustavo Real<div>Carlos Belmar</div>"
        },
        {
          "yaw": -1.3738413405334722,
          "pitch": 0.09121761125950201,
          "title": "4011",
          "text": "Florencia Fiorentín<div>Mariano Pereira</div>"
        }
      ]
    },
    {
      "id": "4-planta-baja---pasillo",
      "name": "Planta baja - Pasillo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 1.6921103643467834,
        "pitch": -0.06043842049182757,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": 1.5718009783680538,
          "pitch": 0.13583805631656176,
          "rotation": 0,
          "target": "5-planta-baja---pasillo"
        },
        {
          "yaw": -1.5800733086494922,
          "pitch": 0.2132494200140158,
          "rotation": 0,
          "target": "3-planta-baja---pasillo"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.8426530212844483,
          "pitch": 0.10281575239802265,
          "title": "4015",
          "text": "Andrea Pinzón<div>Juan Agotegaray</div>"
        },
        {
          "yaw": -2.7489409478689897,
          "pitch": 0.115847023268703,
          "title": "4016",
          "text": "Rubén Cesar<div>Victoria González</div>"
        },
        {
          "yaw": 0.7063302452224605,
          "pitch": 0.08482503088416671,
          "title": "4017",
          "text": "Diego Szlechter<div>Claudio Fardelli Corropolese</div>"
        },
        {
          "yaw": 2.0520520278706478,
          "pitch": 0.05311048161690657,
          "title": "4014",
          "text": "Cecilia Chosco Díaz"
        }
      ]
    },
    {
      "id": "5-planta-baja---pasillo",
      "name": "Planta baja - Pasillo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 1.5733335238080723,
        "pitch": -0.05297826182730603,
        "fov": 1.35820747241737
      },
      "linkHotspots": [
        {
          "yaw": 1.5952825626083724,
          "pitch": 0.18655007099364695,
          "rotation": 0,
          "target": "6-planta-baja---pasillo"
        },
        {
          "yaw": -1.555535764118149,
          "pitch": 0.1911398591875333,
          "rotation": 0,
          "target": "4-planta-baja---pasillo"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 0.41802312418357346,
          "pitch": 0.1276810802487489,
          "title": "4019",
          "text": "Cecilia Allami<div>Arnaldo Ludueña</div><div>Daniela Triador</div>"
        },
        {
          "yaw": 2.5768249212413847,
          "pitch": 0.08836331789334295,
          "title": "4018",
          "text": "Alan Cibils"
        },
        {
          "yaw": -1.8022083053067384,
          "pitch": 0.11650595599959601,
          "title": "4014",
          "text": "Cecilia Chosco Díaz"
        },
        {
          "yaw": -1.358878428194263,
          "pitch": 0.09877132440710845,
          "title": "4017",
          "text": "Diego Szlechter<div>Claudio Fardelli Corropolese</div>"
        }
      ]
    },
    {
      "id": "6-planta-baja---pasillo",
      "name": "Planta baja - Pasillo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": -1.4987253887998815,
        "pitch": 0.08549043225119313,
        "fov": 1.35820747241737
      },
      "linkHotspots": [
        {
          "yaw": -1.5955589818001776,
          "pitch": 0.19653255168410766,
          "rotation": 0,
          "target": "7-planta-baja---pasillo-"
        },
        {
          "yaw": 1.5597775811881753,
          "pitch": 0.13239519277447798,
          "rotation": 0,
          "target": "5-planta-baja---pasillo"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 2.4983294846883792,
          "pitch": 0.06359664386443775,
          "title": "4021",
          "text": "Juan Fal<div>Germán Pinazo</div>"
        },
        {
          "yaw": -2.29351518387665,
          "pitch": 0.09550097769538901,
          "title": "4023",
          "text": "Ernesto Cyrulies<div>Andrés Sartarelli</div>"
        },
        {
          "yaw": -0.915879388561109,
          "pitch": 0.13265074446295344,
          "title": "4020",
          "text": "Sabrina Ibarra García<div>Cecilia Menéndez</div>"
        }
      ]
    },
    {
      "id": "7-planta-baja---pasillo-",
      "name": "Planta baja - Pasillo ",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 1.827332140823776,
        "pitch": 0.047586136323058525,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": 1.6049978419319668,
          "pitch": 0.11382911199561718,
          "rotation": 0,
          "target": "8-planta-baja---pasillo"
        },
        {
          "yaw": -1.5383704513764975,
          "pitch": 0.2006139194511043,
          "rotation": 0,
          "target": "6-planta-baja---pasillo"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -2.3039023918134625,
          "pitch": 0.11915077427798693,
          "title": "4022",
          "text": "Horacio Salomone<div>Néstor Olivieri</div><div>Maximiliano Véliz</div>"
        },
        {
          "yaw": 1.082008061046233,
          "pitch": 0.0643310691273431,
          "title": "4025",
          "text": "Marcelo Mydlarz<div>Javier Martínez Viademonte</div><div>Ivo Koch</div>"
        },
        {
          "yaw": -1.7859287463256095,
          "pitch": 0.09292395413328336,
          "title": "4020",
          "text": "Sabrina Ibarra García<div>Cecilia Menéndez</div>"
        },
        {
          "yaw": -1.2487208369219616,
          "pitch": 0.1251126845882169,
          "title": "4023",
          "text": "Ernesto Cyrulies<div>Andrés Sartarelli</div>"
        }
      ]
    },
    {
      "id": "8-planta-baja---pasillo",
      "name": "Planta baja - Pasillo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 1.702367301176011,
        "pitch": 0.09347997564382915,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": 1.6071706975793116,
          "pitch": 0.1425097612452877,
          "rotation": 0,
          "target": "9-planta-baja---pasillo"
        },
        {
          "yaw": -1.535317666138436,
          "pitch": 0.23595119721836433,
          "rotation": 0,
          "target": "7-planta-baja---pasillo-"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -2.375284260836894,
          "pitch": 0.11700785152470772,
          "title": "4024",
          "text": "Gonzalo Delcauce<div>Jorge Malco</div><div>Alexis Tcach</div><div>Fernando Rages</div>"
        },
        {
          "yaw": 1.0270679716164821,
          "pitch": 0.07601823591166124,
          "title": "4027",
          "text": "Franklin López Medina<div>María Florencia Jauré</div>"
        },
        {
          "yaw": 1.390922331621427,
          "pitch": 0.04251994872230469,
          "title": "4029",
          "text": "Jorge Camblong<div>Sebastián Gatti</div><div>Esteban Acosta</div>"
        }
      ]
    },
    {
      "id": "9-planta-baja---pasillo",
      "name": "Planta baja - Pasillo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 1.5932549313365367,
        "pitch": 0.14285483728554027,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": 1.5824892338920602,
          "pitch": 0.3144899644474073,
          "rotation": 0,
          "target": "10-planta-baja---salida"
        },
        {
          "yaw": -1.524087733368134,
          "pitch": 0.22881414652408694,
          "rotation": 0,
          "target": "8-planta-baja---pasillo"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.8931290462927706,
          "pitch": 0.1043032526969121,
          "title": "4029",
          "text": "Jorge Camblong<div>Sebastián Gatti</div><div>Esteban Acosta</div>"
        },
        {
          "yaw": 1.2883475182893491,
          "pitch": 0.05597151338788464,
          "title": "4031",
          "text": "Natalia Petelski<div>Martín Rodríguez Miglio</div><div>Ignacio Bisso</div>"
        },
        {
          "yaw": 1.5512284273827452,
          "pitch": 0.05201554578407119,
          "title": "4026",
          "text": "Archivo"
        },
        {
          "yaw": -1.2780506436375383,
          "pitch": 0.09778360030543354,
          "title": "4027",
          "text": "Franklin López Medina<div>María Florencia Jauré</div>"
        }
      ]
    },
    {
      "id": "10-planta-baja---salida",
      "name": "Planta baja - Salida",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": -0.17505382828128901,
        "pitch": 0.0702753618202916,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": 2.508639427837485,
          "pitch": 0.15948805840361935,
          "rotation": 4.71238898038469,
          "target": "9-planta-baja---pasillo"
        },
        {
          "yaw": -0.26743480877705217,
          "pitch": 0.27705550455509176,
          "rotation": 0,
          "target": "0-puerta-de-entrada"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -2.9815435907829873,
          "pitch": 0.07698595118722906,
          "title": "4031",
          "text": "Natalia Petelski<div>Martín Rodríguez Miglio</div><div>Ignacio Bisso</div>"
        },
        {
          "yaw": -2.479996363005597,
          "pitch": 0.09082534699725642,
          "title": "4026",
          "text": "Archivo"
        }
      ]
    },
    {
      "id": "11-escalera",
      "name": "Escalera",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 0.7186424372612734,
        "pitch": 0.048471984213280805,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": 0.8471215801065402,
          "pitch": -0.07559767251183303,
          "rotation": 0,
          "target": "12-escalera"
        },
        {
          "yaw": -0.9567160056835391,
          "pitch": 0.8350724144492094,
          "rotation": 0,
          "target": "1-entrada"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "12-escalera",
      "name": "Escalera",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 0.9377069815351753,
        "pitch": -0.14218017992827292,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": 1.217489930233656,
          "pitch": -0.0018714671214929979,
          "rotation": 0,
          "target": "13-planta-alta"
        },
        {
          "yaw": -0.5463823977973377,
          "pitch": 0.866233769738944,
          "rotation": 0,
          "target": "11-escalera"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "13-planta-alta",
      "name": "Planta alta",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 0.04833219467061234,
        "pitch": 0.06598482970240305,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": 0.0882901953564712,
          "pitch": 0.15339512144046097,
          "rotation": 0,
          "target": "14-planta-alta---pasillo"
        },
        {
          "yaw": -1.6951792088537267,
          "pitch": 0.49928546458148304,
          "rotation": 6.283185307179586,
          "target": "12-escalera"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 0.5033296808876493,
          "pitch": 0.11118448307914441,
          "title": "4102",
          "text": "Sala de reuniones"
        }
      ]
    },
    {
      "id": "14-planta-alta---pasillo",
      "name": "Planta alta - Pasillo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 1.6555351974300123,
        "pitch": -0.07957505934196618,
        "fov": 1.35820747241737
      },
      "linkHotspots": [
        {
          "yaw": 1.6138402620575487,
          "pitch": 0.10045202191149016,
          "rotation": 0,
          "target": "15-planta-alta---pasillo"
        },
        {
          "yaw": -1.5375691328711056,
          "pitch": 0.2389145076196293,
          "rotation": 0,
          "target": "13-planta-alta"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.6846908934837792,
          "pitch": 0.23817599831875214,
          "title": "4101",
          "text": "Dirección General de Coordinación Técnico Administrativa (DGCTA)"
        },
        {
          "yaw": -2.4359723528024393,
          "pitch": 0.19359424809021064,
          "title": "4104",
          "text": "Marcelo Neumann<div>Claudio Abrevaya</div>"
        },
        {
          "yaw": 2.208780584421728,
          "pitch": 0.1053556077014175,
          "title": "4106",
          "text": "Hugo Kantis<div>Paola Álvarez Martínez</div>"
        }
      ]
    },
    {
      "id": "15-planta-alta---pasillo",
      "name": "Planta alta - Pasillo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 1.8161514928914766,
        "pitch": 0.05088405691074982,
        "fov": 1.35820747241737
      },
      "linkHotspots": [
        {
          "yaw": 1.6349676086043345,
          "pitch": 0.050644758268116874,
          "rotation": 0,
          "target": "16-planta-alta---pasillo"
        },
        {
          "yaw": -1.5080713765664306,
          "pitch": 0.1552095400495137,
          "rotation": 0,
          "target": "14-planta-alta---pasillo"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -2.5092079074372897,
          "pitch": 0.25044787913569877,
          "title": "4108",
          "text": "Sebastián Sztulwark<div>Pablo Miguez</div>"
        },
        {
          "yaw": 0.7864986979057349,
          "pitch": 0.1740763292102212,
          "title": "4105",
          "text": "Dirección General de Coordinación Técnico Administrativa (DGCTA)"
        },
        {
          "yaw": 1.379904365187075,
          "pitch": 0.07532037247771228,
          "title": "4107",
          "text": "Macarena Ezcurra"
        },
        {
          "yaw": -1.6856652176839866,
          "pitch": 0.1623094742607627,
          "title": "4106",
          "text": "Hugo Kantis<div>Paola Álvarez Martínez</div>"
        }
      ]
    },
    {
      "id": "16-planta-alta---pasillo",
      "name": "Planta alta - Pasillo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": -1.406516140526067,
        "pitch": 0.15203124310106197,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": -1.548734787200651,
          "pitch": 0.22516946624206824,
          "rotation": 0,
          "target": "17-planta-alta---pasillo"
        },
        {
          "yaw": 1.6060062834580897,
          "pitch": 0.00293953128099389,
          "rotation": 0,
          "target": "15-planta-alta---pasillo"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -2.1277378928436494,
          "pitch": 0.21213389208028488,
          "title": "4107",
          "text": "Macarena Ezcurra"
        },
        {
          "yaw": -1.2572175861587542,
          "pitch": 0.24240696470903522,
          "title": "4110",
          "text": "Sonia Roitter<div>Analía Erbes</div>"
        },
        {
          "yaw": 2.4585733517387256,
          "pitch": 0.1098847537218468,
          "title": "4105",
          "text": "Dirección General de Coordinación Técnico Administrativa (DGCTA)"
        },
        {
          "yaw": 1.2429067065405857,
          "pitch": 0.0677778539331424,
          "title": "4108",
          "text": "Sebastián Sztulwark<div>Pablo Miguez</div>"
        }
      ]
    },
    {
      "id": "17-planta-alta---pasillo",
      "name": "Planta alta - Pasillo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 1.7152050193342427,
        "pitch": 0.101615294189914,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": 1.5634624465561267,
          "pitch": 0.04039619760926527,
          "rotation": 0,
          "target": "18-planta-alta---pasillo"
        },
        {
          "yaw": -1.5713022038196307,
          "pitch": 0.15907670302210697,
          "rotation": 0,
          "target": "16-planta-alta---pasillo"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.6771996260331008,
          "pitch": 0.2356983779878643,
          "title": "4107",
          "text": "Macarena Ezcurra"
        },
        {
          "yaw": 2.5194284529546813,
          "pitch": 0.20257250959797624,
          "title": "4110",
          "text": "Sonia Roitter<div>Analía Erbes</div>"
        },
        {
          "yaw": -1.3052691495456248,
          "pitch": 0.15872956755663914,
          "title": "4105",
          "text": "Dirección General de Coordinación Técnico Administrativa (DGCTA)"
        },
        {
          "yaw": 1.7938128897999341,
          "pitch": 0.07275697681401994,
          "title": "4112",
          "text": "Héctor Formento<div>Miguel Languasco</div>"
        },
        {
          "yaw": 1.3589756895333736,
          "pitch": 0.05892009578995783,
          "title": "4109",
          "text": "Néstor Braidot"
        }
      ]
    },
    {
      "id": "18-planta-alta---pasillo",
      "name": "Planta alta - Pasillo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 1.782935016895756,
        "pitch": -0.0497273582550104,
        "fov": 1.35820747241737
      },
      "linkHotspots": [
        {
          "yaw": 1.6532156044901427,
          "pitch": 0.029683581298128203,
          "rotation": 0,
          "target": "19-planta-alta---pasillo"
        },
        {
          "yaw": -1.5025413248236656,
          "pitch": 0.2083462169623722,
          "rotation": 0,
          "target": "17-planta-alta---pasillo"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -1.9294934449195118,
          "pitch": 0.21207339473941644,
          "title": "4110",
          "text": "Sonia Roitter<div>Analía Erbes</div>"
        },
        {
          "yaw": 3.0112091253860083,
          "pitch": 0.18270762423955134,
          "title": "4112",
          "text": "Héctor Formento<div>Miguel Languasco</div>"
        },
        {
          "yaw": 1.1920934065459434,
          "pitch": 0.11146241976597437,
          "title": "4109",
          "text": "Néstor Braidot"
        },
        {
          "yaw": -1.2676508411584315,
          "pitch": 0.19150212520601428,
          "title": "4107",
          "text": "Macarena Ezcurra"
        }
      ]
    },
    {
      "id": "19-planta-alta---pasillo",
      "name": "Planta alta - Pasillo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 1.9053923757860627,
        "pitch": 0.04506538614288402,
        "fov": 1.35820747241737
      },
      "linkHotspots": [
        {
          "yaw": 1.6952442750142165,
          "pitch": 0.07205659676629494,
          "rotation": 0,
          "target": "20-planta-alta---pasillo"
        },
        {
          "yaw": -1.4596801704074132,
          "pitch": 0.14224668139436147,
          "rotation": 0,
          "target": "18-planta-alta---pasillo"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 1.4802283774392988,
          "pitch": 0.0735264203938435,
          "title": "4111",
          "text": "Darío Milesi<div>Vladimiro Verre</div>"
        },
        {
          "yaw": 1.8907172660960212,
          "pitch": 0.0739456513152188,
          "title": "4114",
          "text": "Jorge Nicolini<div>Sandra Hernández</div>"
        },
        {
          "yaw": 0.34407634844777846,
          "pitch": 0.22258878285646588,
          "title": "4109",
          "text": "Néstor Braidot"
        },
        {
          "yaw": -2.1482454811761276,
          "pitch": 0.19136591861303032,
          "title": "4112",
          "text": "Héctor Formento<div>Miguel Languasco</div>"
        }
      ]
    },
    {
      "id": "20-planta-alta---pasillo",
      "name": "Planta alta - Pasillo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 1.6141828851273274,
        "pitch": 0.0624945976204927,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": 1.573452386638058,
          "pitch": 0.06740614879642415,
          "rotation": 0,
          "target": "21-planta-alta---pasillo"
        },
        {
          "yaw": -1.562465108843103,
          "pitch": 0.1481652446131676,
          "rotation": 0,
          "target": "18-planta-alta---pasillo"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.7459594130939937,
          "pitch": 0.23458781711313748,
          "title": "4111",
          "text": "Darío Milesi<div>Vladimiro Verre</div>"
        },
        {
          "yaw": -2.344777077548395,
          "pitch": 0.18898552543322822,
          "title": "4114",
          "text": "Jorge Nicolini<div>Sandra Hernández</div>"
        },
        {
          "yaw": 2.164864557749124,
          "pitch": 0.1279986364070158,
          "title": "4116",
          "text": "Juan Federico<div>Manuel Gonzalo</div>"
        },
        {
          "yaw": 0.9227423728909709,
          "pitch": 0.16849502134473937,
          "title": "4113",
          "text": "Gustavo Jiménez Placer<div>Osvaldo Vitali</div><div>Daniel Zambrano</div><div>Enrique Modai</div><div>Daniel Monferrán</div>"
        }
      ]
    },
    {
      "id": "21-planta-alta---pasillo",
      "name": "Planta alta - Pasillo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 1.7584246301036242,
        "pitch": 0.04497045551857681,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": 1.658590418208405,
          "pitch": 0.07567130088522411,
          "rotation": 0,
          "target": "22-planta-alta---pasillo"
        },
        {
          "yaw": -1.4839561989053909,
          "pitch": 0.13921317549702295,
          "rotation": 0,
          "target": "20-planta-alta---pasillo"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -2.1677747855738225,
          "pitch": 0.21570891527709257,
          "title": "4118",
          "text": "Oscar Ramírez<div>María Fernanda Ferreira</div>"
        },
        {
          "yaw": 1.032612881841807,
          "pitch": 0.12737991266809523,
          "title": "4115",
          "text": "Marcelo O. Fernández<div>Fernando Cusolito</div>"
        },
        {
          "yaw": 2.4080749499606684,
          "pitch": 0.15318765189817185,
          "title": "4120",
          "text": "Miguel Benegas<div>Rocío Rosatti</div>"
        }
      ]
    },
    {
      "id": "22-planta-alta---pasillo",
      "name": "Planta alta - Pasillo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 1.7486898223301521,
        "pitch": 0.127911776439932,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": 1.6015847704050659,
          "pitch": 0.06114409430428758,
          "rotation": 0,
          "target": "23-planta-alta---pasillo"
        },
        {
          "yaw": -1.5379343825858243,
          "pitch": 0.17427932869005858,
          "rotation": 0,
          "target": "21-planta-alta---pasillo"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -2.1176419090112173,
          "pitch": 0.19293358487063372,
          "title": "4122",
          "text": "Natalia González<div>Daiana Díaz</div>"
        },
        {
          "yaw": 1.2606341588307188,
          "pitch": 0.06122651733664952,
          "title": "4117",
          "text": "Gustavo Seijo<div>Javier Cantero</div><div>Leopoldo Blugerman</div><div>Paloma Fidmay</div><div>Marina Calamari</div><div>Maximiliano Ozono</div>"
        }
      ]
    },
    {
      "id": "23-planta-alta---pasillo",
      "name": "Planta alta - Pasillo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": -1.412578896279138,
        "pitch": 0.14826618268761038,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": -1.5558109311172146,
          "pitch": 0.1709535851274122,
          "rotation": 0,
          "target": "24-planta-alta---pasillo"
        },
        {
          "yaw": 1.6002053896461028,
          "pitch": 0.07354937230262948,
          "rotation": 0,
          "target": "22-planta-alta---pasillo"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 2.4515777931794798,
          "pitch": 0.13961125504240535,
          "title": "4117",
          "text": "Gustavo Seijo<div>Javier Cantero</div><div>Leopoldo Blugerman</div><div>Paloma Fidmay</div><div>Marina Calamari</div><div>Maximiliano Ozono</div>"
        },
        {
          "yaw": -1.2572473577650207,
          "pitch": 0.1817702372129837,
          "title": "4124",
          "text": "Sala de reuniones"
        },
        {
          "yaw": -1.9310003394933517,
          "pitch": 0.13364090627310254,
          "title": "4119",
          "text": "Gabriel Yoguel<div>Diana Suárez</div>"
        }
      ]
    },
    {
      "id": "24-planta-alta---pasillo",
      "name": "Planta alta - Pasillo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": -1.4547879842057903,
        "pitch": 0.1255578660590082,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": -1.528427707858345,
          "pitch": 0.16120312423476157,
          "rotation": 0,
          "target": "25-planta-alta---pasillo"
        },
        {
          "yaw": 1.61875957754558,
          "pitch": 0.08107335666913862,
          "rotation": 0,
          "target": "23-planta-alta---pasillo"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 0.3866970955292999,
          "pitch": 0.24503375791802284,
          "title": "4124",
          "text": "Sala de reuniones"
        },
        {
          "yaw": 2.9416468068412343,
          "pitch": 0.19907258785425697,
          "title": "4119",
          "text": "Gabriel Yoguel<div>Diana Suárez</div>"
        },
        {
          "yaw": 1.8462832824958424,
          "pitch": 0.044701637261999494,
          "title": "4117",
          "text": "Gustavo Seijo<div>Javier Cantero</div><div>Leopoldo Blugerman</div><div>Paloma Fidmay</div><div>Marina Calamari</div><div>Maximiliano Ozono</div>"
        }
      ]
    },
    {
      "id": "25-planta-alta---pasillo",
      "name": "Planta alta - Pasillo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 1.6515173526537232,
        "pitch": -0.015121372950533285,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": 1.6235426014092678,
          "pitch": 0.11482442856567943,
          "rotation": 0,
          "target": "26-planta-alta---salida"
        },
        {
          "yaw": -1.510041512648094,
          "pitch": 0.1823402452717957,
          "rotation": 0,
          "target": "24-planta-alta---pasillo"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "26-planta-alta---salida",
      "name": "Planta alta - Salida",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": -1.4969958515992374,
        "pitch": 0.12217307741156525,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": -1.55290496992572,
          "pitch": 0.2228082219108991,
          "rotation": 0,
          "target": "0-puerta-de-entrada"
        },
        {
          "yaw": 1.6089565579260583,
          "pitch": 0.12845045497035557,
          "rotation": 0,
          "target": "25-planta-alta---pasillo"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.3718125461531372,
          "pitch": 0.30569658432069957,
          "title": "4126",
          "text": "Fotocopiadora e impresoras"
        }
      ]
    }
  ],
  "name": "Recorrido IDEI",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": false,
    "fullscreenButton": true,
    "viewControlButtons": false
  }
};
